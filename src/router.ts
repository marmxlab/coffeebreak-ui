import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home.vue';
import Game from './views/Game.vue';
import { loadLanguageAsync } from '@/setup/i18n-setup';

Vue.use(Router);

const router = new Router({
  routes: [
    {
      path: '/',
      component: Home,
    },
    {
      path: '/game',
      component: Game,
    },
  ],
});

router.beforeEach((to, from, next) => {
  const lang = to.query.lang;

  if (lang) {
    loadLanguageAsync(lang).then(() => next());
  } else {
    next();
  }
});

export default router;
