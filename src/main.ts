import Vue from 'vue';
import VueI18n from 'vue-i18n';
import App from '@/App.vue';
import router from '@/router';
import store from '@/store';
import { i18n } from '@/setup/i18n-setup';
import 'reset-css/reset.css';
import 'animate.css/animate.min.css';
import 'vue-awesome/icons/times';
import Icon from 'vue-awesome/components/Icon';
import WebsocketUtil from "@/utils/websocket";

WebsocketUtil.connect().catch((e: any) => {
  console.error(e);
});

Vue.config.productionTip = false;

Vue.use(VueI18n);

Vue.component('v-icon', Icon)

new Vue({
  router,
  store,
  i18n,
  render: (h) => h(App),
}).$mount('#app');
