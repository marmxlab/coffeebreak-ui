import Session from '@/models/session';

const GAME_ID = 'GAME_ID';
const GAME_SESSION = 'GAME_SESSION';

export default class StorageUtil {
  static setGame(gameId: number, session: Session) {
    localStorage.setItem(GAME_ID, gameId.toString());
    localStorage.setItem(GAME_SESSION, JSON.stringify(session));
  }

  static getGame() : { gameId: number | null, session: Session | null } {
    const gameId = localStorage.getItem(GAME_ID);
    const session = localStorage.getItem(GAME_SESSION);

    return {
      gameId: gameId !== null ? parseInt(gameId) : null,
      session: session !== null ? JSON.parse(session) : null,
    };
  }

  static clear() {
    localStorage.clear();
  }
}
