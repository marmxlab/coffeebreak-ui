import axios, { AxiosPromise } from "axios";

const GAME_LIST_API = "/api/gameList";

export default class APIUtil {
  static getGameList(): AxiosPromise {
    return axios.get(GAME_LIST_API);
  }
}
