import { Actions } from '@/enums/actions';
import store from '@/store';
import Session from '@/models/session';

export default class WebSocketUtil {
  public static connect(): Promise<void> {
    return new Promise<void>((resolve, reject) => {
      this.client.connect((error: any) => {
        if (error) {
          reject(error);
        }

        store.dispatch('wsStatusChange', { status: true });

        this.client.on('message', (message: any) => {
          // console.log(message);
        });

        this.client.on('disconnected', () => {
          store.dispatch('wsStatusChange', { status: false });
        });

        resolve();
      });
    });
  }

  public static createRoom(name: string, colour: number, cb: (response: any) => void) {
    this.send(Actions.GAME_CREATE, { name, colour }, cb);
  }
  public static joinRoom(gameId: number, name: string, colour: number, cb: (response: any) => void) {
    this.send(Actions.GAME_JOIN, { gameId, name, colour }, cb);
  }

  public static startGame(gameId: number, cb?: (response: any) => void) {
    const { playerId, sessionKey } = this.getSession();
    this.send(Actions.GAME_START, { gameId, playerId, sessionKey }, cb);
  }

  // @ts-ignore
  private static readonly client: any = new ActionheroWebsocketClient({
    url: 'http://localhost:8081'
  });

  private static getSession(): Session {
    return store.getters.session;
  }
  private static send(action: string, data: any, cb?: (data: any) => void) {
    this.client.action(action, data, cb);
  }
}
