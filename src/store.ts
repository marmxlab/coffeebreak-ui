import Vue from "vue";
import Vuex from "vuex";
import { Player } from "@/models/player";
import Session from "@/models/session";
import StorageUtil from "@/utils/storage";

Vue.use(Vuex);

export interface RootState {
  wsConnected: boolean;
  gameId?: number;
  session?: Session;
  players: Player[];
}

export default new Vuex.Store<RootState>({
  state: {
    wsConnected: false,
    gameId: undefined,
    session: undefined,
    players: [],
  },
  mutations: {
    wsStatusChange(state, { status }) {
      state.wsConnected = status;
    },

    joinGame(state, { gameId, session, players }) {
      state.gameId = gameId;
      state.session = session;

      if (players) {
        state.players = players;
      }
    }
  },
  actions: {
    wsStatusChange({ commit }, { status }) {
      commit("wsStatusChange", { status });
    },
    joinGame({ commit }, { gameId, session, players }) {
      StorageUtil.setGame(gameId, session);
      commit("joinGame", { gameId, session, players });
    },
  },
  getters: {
    isGameOwner({ session, players }): boolean {
      return players.length > 0 && session ? players[0].id === session.playerId : false;
    },
    gameId({ gameId }): number | undefined {
      return gameId;
    },
    players({ players }): Player[] {
      return players ? players : [];
    },
    session({ session }): Session | undefined {
      return session;
    },
    wsConnected({ wsConnected }) {
      return wsConnected;
    }
  }
});
