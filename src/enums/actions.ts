export enum Actions {
  GAME_LIST = "gameList",
  GAME_CREATE = "gameCreate",
  GAME_JOIN = "gameJoin",
  GAME_START = "gameStart"
}
