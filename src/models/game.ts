export class Game {
  public readonly id: number;
  public numOfPlayers: number;
  public status: Status;

  constructor(id: number, numOfPlayers: number, status: Status) {
    this.id = id;
    this.numOfPlayers = numOfPlayers;
    this.status = status;
  }
}

export enum Status {
  STARTED = 'STARTED',
  WAITING = 'WAITING',
}
