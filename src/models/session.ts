export default class Session {
  public playerId: number;
  public sessionKey: string;

  constructor(playerId: number, sessionKey: string) {
    this.playerId = playerId;
    this.sessionKey = sessionKey;
  }
}
