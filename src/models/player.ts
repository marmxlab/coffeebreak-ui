export class Player {
  public readonly id: number;
  public readonly name: string;
  public readonly color: Color;

  constructor(id: number, name: string, color: Color) {
    this.id = id;
    this.name = name;
    this.color = color;
  }
}

export enum Color {
  YELLOW, WHITE, RED, BLUE, GREEN, PINK,
}
