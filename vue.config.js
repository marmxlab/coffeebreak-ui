var path = require('path');

module.exports = {
  css: {
    loaderOptions: {
      stylus: {
        import: [
          path.resolve(__dirname, 'src/stylus/variables.styl'),
          path.resolve(__dirname, 'src/stylus/global.styl')
        ],
      }
    }
  },
  configureWebpack: {
    devServer: {
      proxy: {
        '/api': {
          target: 'http://localhost:8081'
        }
      }
    }
  }
};
